MAX_SIZE = 15;

mask = zeros(MAX_SIZE);

CENTRAL_X = ceil(MAX_SIZE / 2);
CENTRAL_Y = ceil(MAX_SIZE / 2);

for i=1:MAX_SIZE
    for j=1:MAX_SIZE
        mask(i, j) = (CENTRAL_X - i) ^ 2 + (CENTRAL_Y - j) ^ 2;
    end
end

mask = uint8(mask ./ max(max(mask)) * 255);