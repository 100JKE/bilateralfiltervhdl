import serial
from PIL import Image
import numpy as np

ser = serial.Serial(
    port='COM21',
    baudrate=115200
)

#im = Image.open('cameraman_noise.jpg')
im = Image.open('original.jpg');

w = im.size[0]
h = im.size[1]

ret = np.zeros((w,h)) 

# Skip first 128 pixels which are garbage.
i = 0;
while i < w * h + 128:
	if i < w * h:
		pix =  im.getpixel(( i % w, i / w  ))
	ser.write(chr(pix))
	if i >= 128:
		ret[(i - 128) % w][(i - 128) / w] = ord(ser.read(1))
	else:
		ord(ser.read(1))
	i = i + 1
	print (1.0 * i / (w * h + 128)) * 100

outim = Image.fromarray(ret.astype(np.uint8).transpose())
outim.save('izlaz.bmp')
