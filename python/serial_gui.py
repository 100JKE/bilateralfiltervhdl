import wx

import os
import threading

import serial
from PIL import Image
import numpy as np

MASK_SIZE = 5

class CommThread(threading.Thread):
    def __init__(self, inputPath, outputPath, toUpdate):
        threading.Thread.__init__(self)
        self.percent = 0
        self.toUpdate = toUpdate
        self.imagePath = inputPath
        self.outputPath = outputPath

    def run(self):
        ser = serial.Serial(
            port='/dev/ttyUSB0',
            baudrate=115200
        )

        self.percent = 0

        im = Image.open(self.imagePath)

        # Symmetrical pad image
        arrIm = np.array(im)
        arrIm = np.lib.pad(arrIm, ( MASK_SIZE - 1 ) / 2, 'symmetric')

        im = Image.fromarray(arrIm)

        w = im.size[0]
        h = im.size[1]

        ret = np.zeros((w, h))

        # Skip first 128 pixels which are garbage.
        offset = 128 + MASK_SIZE * w;
        i = 0
        while i < w * h + offset:
            if i < w * h:
                pix = im.getpixel((i % w, i / w))
            ser.write(chr(pix))
            if i >= offset:
                ret[(i - offset) % w][(i - offset) / w] = ord(ser.read(1))
            else:
                ord(ser.read(1))
            i += 1
            self.percent = (1.0 * i / (w * h + 128)) * 100
            self.toUpdate.update(self.percent)

        outim = Image.fromarray(ret.astype(np.uint8).transpose())

        # Crop image back to original size
        outim = outim.crop((MASK_SIZE / 2, MASK_SIZE / 2, im.size[0] - MASK_SIZE / 2, im.size[1] - MASK_SIZE / 2))

        outim.save(self.outputPath)
        self.toUpdate.finish()

        ser.close()


class MyForm(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Bilateral Filter", size=(650, 550))
        self.SetMaxSize(self.GetSize())
        self.SetMinSize(self.GetSize())

        self.inputImagePath = ""
        self.outputImagePath = ""

        self.img_default_w = 300
        self.img_default_h = 300

        # Add a panel so it looks the correct on all platforms
        self.panel = wx.Panel(self, wx.ID_ANY)

        self.btInput = wx.Button(self.panel, 1, "Input image")
        self.btOutput = wx.Button(self.panel, 2, "Output image")

        self.btInput.Bind(wx.EVT_LEFT_DOWN, self.chose_input_file)
        self.btOutput.Bind(wx.EVT_LEFT_DOWN, self.chose_output_file)

        img = wx.EmptyImage(self.img_default_w, self.img_default_h)
        self.imageInput = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.BitmapFromImage(img))
        self.imageOutput = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.BitmapFromImage(img))

        self.btStart = wx.Button(self.panel, 3, "START")
        self.btStart.Disable()
        self.progress = wx.Gauge(self.panel, 100)
        self.btStart.Bind(wx.EVT_LEFT_DOWN, self.on_start)


        # Layout
        self.sizerIO = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerIO.Add(self.btInput, 1, wx.EXPAND | wx.ALL, border=10)
        self.sizerIO.Add(self.btOutput, 1, wx.EXPAND | wx.ALL, border=10)


        self.sizerImgs = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerImgs.Add(self.imageInput, 1, wx.CENTER | wx.ALL, border=10)
        self.sizerImgs.Add(self.imageOutput, 1, wx.CENTER | wx.ALL, border=10)

        self.sizerStart = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerStart.Add(self.btStart, 1, wx.EXPAND, border=20)
        self.sizerStart.Add(self.progress, 5, wx.EXPAND, border=20)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.sizerIO, 1, wx.EXPAND)
        self.sizer.Add(self.sizerImgs, 4, wx.CENTER)
        self.sizer.Add(self.sizerStart, 1, wx.EXPAND)

        self.panel.SetSizer(self.sizer)

    def chose_input_file(self, event):
        openFileDialog = wx.FileDialog(self, "Open", os.getcwd(), "",
                                       "All files (*.*)|*.*",
                                       wx.FD_OPEN)

        if openFileDialog.ShowModal() == wx.ID_OK:
            self.inputImagePath = openFileDialog.GetPath()
            img = wx.Image(self.inputImagePath, wx.BITMAP_TYPE_ANY)
            self._resize_based_on_img(img)
            self.imageOutput.SetBitmap(wx.BitmapFromImage(wx.EmptyImage(self.img_default_w, self.img_default_h)))
            self.imageInput.SetBitmap(wx.BitmapFromImage(img))
            self.panel.Refresh()
            print openFileDialog.GetPath()

        openFileDialog.Destroy()

    def _resize_based_on_img(self, img):
        w = self.GetMaxWidth() + (img.Width - self.img_default_w)
        h = self.GetMaxHeight() + (img.Height - self.img_default_h)
        self.img_default_w = img.Width
        self.img_default_h = img.Height
        self.SetMaxSize((2 * w, 2 * h))
        self.SetMinSize((w/2, h/2))
        self.SetSize((w, h))
        self.SetMaxSize((w, h))
        self.SetMinSize((w, h))

    def chose_output_file(self, event):
        saveFileDialog = wx.FileDialog(self, "Save As", os.getcwd(), "",
                                       "All files (*.*)|*.*",
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if saveFileDialog.ShowModal() == wx.ID_OK:
            self.outputImagePath = saveFileDialog.GetPath()
            self.btStart.Enable()
            print saveFileDialog.GetPath()

        saveFileDialog.Destroy()

    def on_start(self, event):
        self.progress.SetValue(0)
        self.comm = CommThread(self.inputImagePath, self.outputImagePath, self)
        self.comm.start()

    def update(self, percent):
        # Multi thread access to wx GUI object can't be done in asynchronous manner.
        # CallAfter must be used to let wx backend do de required job at appropriate moment.
        wx.CallAfter(self.progress.SetValue, percent)

    def finish(self):
        img = wx.Image(self.outputImagePath, wx.BITMAP_TYPE_ANY)
        self.imageOutput.SetBitmap(wx.BitmapFromImage(img))
        self.panel.Refresh()
        self.progress.SetValue(0)

app = wx.App(False)
frame = MyForm()
frame.Show()
app.MainLoop()
