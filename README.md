# Bilateral filter implementation in VHDL #

Efficient [image bilateral filtering](https://en.wikipedia.org/wiki/Bilateral_filter) implementation in VHDL with debugging and visualization tools.

Project was developed in Altera Quartus II and deployed on Cyclone V family FPGA chip.

Authors: Miloš Stojanović and Vladimir Polovina. Authors contributed equally.