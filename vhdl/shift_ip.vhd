library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

entity shift_ip is
	port
	(
		clk								: in 	STD_LOGIC ;
		shift_in							: in 	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		shift_out						: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		taps								: out STD_LOGIC_VECTOR ( DATA_SIZE * MASK_SIZE - 1 downto 0 )
	);
end shift_ip;


architecture shift_ip_arch of shift_ip is

	component altshift_taps is
	generic
	(
		intended_device_family		: STRING;
		lpm_hint							: STRING;
		lpm_type							: STRING;
		number_of_taps					: NATURAL;
		tap_distance					: NATURAL;
		width								: NATURAL
	);
	port
	(
			clock							: in 	STD_LOGIC ;
			shiftin						: in 	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
			shiftout						: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
			taps							: out STD_LOGIC_VECTOR ( DATA_SIZE * MASK_SIZE - 1 downto 0 )
	);
	end component altshift_taps;
	
	signal sub_wire0					: STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
	signal sub_wire1					: STD_LOGIC_VECTOR ( DATA_SIZE * MASK_SIZE - 1 downto 0 );
	signal count						: INTEGER := 0;

begin

	shift_out <= sub_wire0 ( DATA_SIZE - 1 downto 0 );
	taps <= sub_wire1 ( DATA_SIZE * MASK_SIZE - 1 downto 0 );

	GENERATE_ALTSHIFT_TAPS : altshift_taps
										generic map
										(
											intended_device_family	=> "Cyclone IV E",
											lpm_hint 					=> "RAM_BLOCK_TYPE=AUTO",
											lpm_type 					=> "altshift_taps",
											number_of_taps 			=> MASK_SIZE,
											tap_distance 				=> EXT_IMAGE_WIDTH,
											width 						=> DATA_SIZE
										)
										port map
										(
											clock 						=> clk,
											shiftin 						=> shift_in,
											shiftout 					=> sub_wire0,
											taps 							=> sub_wire1
										);
		
end shift_ip_arch;
