-----------------------------------------------------------------------------
--! @file bilateral.vhd
--! @brief Bilateral filter for image processing.
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

-----------------------------------------------------------------------------
--! @brief Bilateral filter for image processing.
--!
--! Biltateral filter is image processing algorithm used for enhancing the 
--! quality of noisy image but retaining the quality of the edges. 
--! This is paralele implementation targeted to enable one output pixel per 
--! clock cycle.
--! Image and mask size can be set in commons.vhd package. Standard 
--! deviations (defined by the filter) algorithm can be set via inpus signals
-----------------------------------------------------------------------------
entity bilateral is
	port 
	(
		--! Clock
		clk								: in	STD_LOGIC; 
		--! Standard deviation of Gausian distribution for distance from the center pixel
		sigma_s							: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		--! Standard deviation of Gausian distribution for the pixel difference
		sigma_r							: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		--! Pixel inputed as a stream of pixels
		data_in							: in	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		--! Pixel of filtered image outputed as a stream of pixels
		data_out							: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 )
	);
end entity;

architecture bilateral_arch of bilateral is

	component shift_ip is
	port
	(
		clk								: in 	STD_LOGIC ;
		shift_in							: in 	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		shift_out						: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		taps								: out STD_LOGIC_VECTOR ( DATA_SIZE * MASK_SIZE - 1 downto 0 )
	);
	end component shift_ip;
	
	component shift_reg is
	port 
	(
		clk								: in 	STD_LOGIC;
		data								: in 	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		sr_out							: out STD_LOGIC_VECTOR ( MASK_SIZE * DATA_SIZE - 1 downto 0 )
	);	
	end component shift_reg;
	
	component compute is
	port
	(
		window							: in 	taps_matrix;
		sigma_s							: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_r							: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		output_pixel 					: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 )
	);
	end component compute;
	
	signal 	mask 						: taps_matrix; 
	signal 	i_taps 					: STD_LOGIC_VECTOR ( DATA_SIZE * MASK_SIZE - 1 downto 0 );

begin

	GENERATE_SHIFT_IP	:	component shift_ip
									port map
									(
										clk 					=> clk,
										shift_in 			=> data_in,
										shift_out 			=> open,
										taps 					=> i_taps
									);
			
	GENERATE_MASK:	for I in MASK_SIZE - 1 downto 0 generate
		SF_REG:	component shift_reg 
						port map
						(
							clk 								=> clk,
							data 								=> i_taps( ( I + 1 ) * DATA_SIZE - 1 downto I * DATA_SIZE ),
							sr_out 							=> mask( I )
						);
	end generate GENERATE_MASK;
	
	GENERATE_COMPUTE :	component compute
									port map
									(
										window 				=> mask,
										sigma_s				=> sigma_s,
										sigma_r				=> sigma_r,
										output_pixel 		=> data_out
									);
	
end bilateral_arch;