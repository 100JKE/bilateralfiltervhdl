-----------------------------------------------------------------------------
--! @file compute.vhd
--! @brief Compution of bilateral filter output for a given mask (window)
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

-----------------------------------------------------------------------------
--! @brief Compute the output pixel based on convolution mask
--!
--! One output pixel is generated from inputed convolution mask by applaying
--! bilateral filter algorithm to the mask.
--!
--! The steps are:
--! 	- Inputed sigmas are rescaeld
--!	- Wheights are generated from lut's based on pixel data in window
--!	- Window of input pixels is multiplied by generated wheight matrix
--!	- Multiplication result is summed using the adder tree
--!	- Generated wheight matrix is summed using the adder tree
--!	- Outptus of these two summations are divided to get fina pixel value.
-----------------------------------------------------------------------------
entity compute is
	port
	(
		window						: in 	taps_matrix;
		sigma_s						: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_r						: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		output_pixel 				: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 )
	);
end compute;

architecture compute_arch of compute is

	component sigma_scaling is
	port 
	(
		sigma_s						: in 	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_r						: in 	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_s_scaled				: out INTEGER;
		sigma_r_scaled 			: out INTEGER
		
	);
	end component sigma_scaling;

	component look_up is
		port 
		(
			address1					: in 	INTEGER;
			address2					: in 	INTEGER;
			sigma_s_scaled			: in	INTEGER;
			sigma_r_scaled			: in	INTEGER;
			data						: out INTEGER
		);
	end component look_up;
	
   component sub is
	port 
	(
		in1							: in 	INTEGER;
		in2							: in 	INTEGER;
		result						: out INTEGER
	);
	end component sub;
	
	component mul is
	port 
	(
		in1							: in 	INTEGER;
		in2							: in 	INTEGER;
		result						: out INTEGER
	);
	end component mul;
	
	component adder_tree is
	port 
	(
		in_array						: in 	row;
		sum							: out INTEGER
	);
	end component adder_tree;
	
	component divide_ip is
	port
	(
		denom							: in 	STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
		numer							: in 	STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
		quotient						: out STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
		remain						: out STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 )
	);
	end component divide_ip;
	
	signal 	central_pixel 		: STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
	
	signal	i_sigma_s_scaled	: INTEGER;
	signal	i_sigma_r_scaled	: INTEGER;

	constant CENTRAL_INDEX 		: NATURAL := ( MASK_SIZE - 1 ) / 2;
	constant OFFSET				: NATURAL := ( MAX_MASK_SIZE - MASK_SIZE ) / 2;
	
	signal 	difference 			: matrix;
	signal 	lut_out				: matrix;
	
	type 		row_MAX 		is array ( MAX_MASK_SIZE - 1 downto 0 ) of INTEGER;
	type 		matrix_MAX 	is array ( MAX_MASK_SIZE - 1 downto 0 ) of row_MAX;
	
	signal 	dist_lut			: matrix_MAX := ( ( 14, 13, 12, 11, 10, 9, 8, 7, 8, 9, 10, 11, 12, 13, 14 ),
															( 13, 12, 11, 10,  9, 8, 7, 6, 7, 8,  9, 10, 11, 12, 13 ),
															( 12, 11, 10,  9,  8, 7, 6, 5, 6, 7,  8,  9, 10, 11, 12 ),
															( 11, 10,  9,  8,  7, 6, 5, 4, 5, 6,  7,  8,  9, 10, 11 ),
															( 10,  9,  8,  7,  6, 5, 4, 3, 4, 5,  6,  7,  8,  9, 10 ),
															(  9,  8,  7,  6,  5, 4, 3, 2, 3, 4,  5,  6,  7,  8,  9 ),
															(  8,  7,  6,  5,  4, 3, 2, 1, 2, 3,  4,  5,  6,  7,  8 ),
															(  7,  6,  5,  4,  3, 2, 1, 0, 1, 2,  3,  4,  5,  6,  7 ),
															(  8,  7,  6,  5,  4, 3, 2, 1, 2, 3,  4,  5,  6,  7,  8 ),
															(  9,  8,  7,  6,  5, 4, 3, 2, 3, 4,  5,  6,  7,  8,  9 ),
															( 10,  9,  8,  7,  6, 5, 4, 3, 4, 5,  6,  7,  8,  9, 10 ),
															( 11, 10,  9,  8,  7, 6, 5, 4, 5, 6,  7,  8,  9, 10, 11 ),
															( 12, 11, 10,  9,  8, 7, 6, 5, 6, 7,  8,  9, 10, 11, 12 ),
															( 13, 12, 11, 10,  9, 8, 7, 6, 7, 8,  9, 10, 11, 12, 13 ),
															( 14, 13, 12, 11, 10, 9, 8, 7, 8, 9, 10, 11, 12, 13, 14 ) );
	
	signal 	mul_out			: matrix;
	signal 	sum				: row;
	signal 	sum_weights		: row;
	signal 	sum_fin			: INTEGER;
	signal 	sum_weights_fin: INTEGER;
	
	signal 	i_denom 			: STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
	signal 	i_numer 			: STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
	signal	i_quotient 		: STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
	
begin

	central_pixel <= window( CENTRAL_INDEX )( ( CENTRAL_INDEX + 1 ) * DATA_SIZE - 1 downto CENTRAL_INDEX * DATA_SIZE );
	
	GENERATE_SIGMA_SCALING:	component sigma_scaling
										port map
										(
												sigma_s			=> sigma_s,
												sigma_r			=> sigma_r,
												sigma_s_scaled	=> i_sigma_s_scaled,
												sigma_r_scaled => i_sigma_r_scaled
										);										
	
	GENERATE_DIFF_OUT: for I in MASK_SIZE - 1 downto 0 generate
		GENERATE_DIFF_IN:	for J in MASK_SIZE - 1 downto 0 generate
									SUBX: component sub
										port map
										(
											in1 					=> TO_INTEGER( UNSIGNED( window( I )( ( J + 1 ) * DATA_SIZE - 1 downto J * DATA_SIZE ) ) ),
											in2 					=> TO_INTEGER( UNSIGNED( central_pixel ) ),
											result 				=> difference( I )( J )
										);
		end generate GENERATE_DIFF_IN;
	end generate GENERATE_DIFF_OUT;
	
	GENERATE_LUT_OUT: for I in MASK_SIZE - 1 downto 0 generate
		GENERATE_LUT_IN:	for J in MASK_SIZE - 1 downto 0 generate
									LUTX: component look_up
										port map
										(
											address1 			=> dist_lut( OFFSET + I )( OFFSET + J ),
											address2 			=> difference( I )( J ),
											sigma_s_scaled		=> i_sigma_s_scaled,
											sigma_r_scaled		=> i_sigma_r_scaled,
											data					=> lut_out( I )( J )
										);
		end generate GENERATE_LUT_IN;
	end generate GENERATE_LUT_OUT; 
	
	GENERATE_MUL_OUT: for I in MASK_SIZE - 1 downto 0 generate
		GENERATE_MUL_IN:	for J in MASK_SIZE - 1 downto 0 generate
									LUTX: component mul
										port map
										(
											in1 					=> lut_out( I )( J ),
											in2 					=> TO_INTEGER( UNSIGNED( window( I )( ( J + 1 ) * DATA_SIZE - 1 downto J * DATA_SIZE ) ) ),
											result 				=> mul_out( I )( J )
										);
		end generate GENERATE_MUL_IN;
	end generate GENERATE_MUL_OUT;
		
	GENERATE_ADDER_TREE:	for I in MASK_SIZE - 1 downto 0 generate
									ADDX:	component adder_tree
										port map
										(
											in_array 			=> mul_out( I ),
											sum 					=> sum( I )
										);
	end generate GENERATE_ADDER_TREE;	
	
	GENERATE_FINAL_ADDER_TREE: component adder_tree
											port map
											(
												in_array			=> sum,
												sum 				=> sum_fin
											);
										
	GENERATE_WEIGHTS_ADDER_TREE:	for I in MASK_SIZE - 1 downto 0 generate
												ADDX:	component adder_tree
													port map
													(
														in_array	=> lut_out( I ),
														sum 		=> sum_weights( I )
													);
	end generate GENERATE_WEIGHTS_ADDER_TREE;	
	
	GENERATE_FINAL_WEIGHTS_ADDER_TREE: component adder_tree
													port map
													(
														in_array => sum_weights,
														sum 		=> sum_weights_fin
													);
	GENERATE_DIVIDER : component divide_ip
								port map 
								(
									denom	 						=> i_denom,
									numer	 						=> i_numer,
									quotient 					=> i_quotient,
									remain 						=> open
								);
		
	process( sum_fin, sum_weights_fin, i_quotient )
	begin
			i_numer <= STD_LOGIC_VECTOR( TO_UNSIGNED( sum_fin, EXT_DATA_SIZE ) );
			i_denom <= STD_LOGIC_VECTOR( TO_UNSIGNED( sum_weights_fin, EXT_DATA_SIZE ) );
			output_pixel <= i_quotient( DATA_SIZE - 1 downto 0 ); 
	end process;

end compute_arch;
