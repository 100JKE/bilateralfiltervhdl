-----------------------------------------------------------------------------
--! @file shift_reg.vhd
--! @brief Simple shift register used for creating convolution mask.
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

entity shift_reg is
	port 
	(
		clk			: in 	STD_LOGIC;
		data			: in 	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		sr_out		: out STD_LOGIC_VECTOR ( MASK_SIZE * DATA_SIZE - 1 downto 0 )
	);

end entity;

architecture shift_reg_arch of shift_reg is

	signal sr		: UNSIGNED (MASK_SIZE * DATA_SIZE - 1 downto 0);
	
begin

	process( clk )
	begin
		if( rising_edge( clk ) ) then
			sr <= UNSIGNED( data ) & sr( MASK_SIZE * DATA_SIZE - 1 downto DATA_SIZE );
		end if;
	end process;

	sr_out <= STD_LOGIC_VECTOR( sr );

end shift_reg_arch;
