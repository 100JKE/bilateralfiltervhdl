-----------------------------------------------------------------------------
--! @file sub.vhd
--! @brief Absolute difference betweene pixle values
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

entity sub is
	port 
	(
		in1			: in 	INTEGER;
		in2			: in 	INTEGER;
		result		: out INTEGER
	);
end entity;

architecture sub_arch of sub is
begin

		process( in1, in2 )
		begin
			if( in1 < in2 ) then
				result <= in2 - in1;
			else
				result <= in1 - in2;
			end if;
		end process;
		
end sub_arch;
