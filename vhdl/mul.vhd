-----------------------------------------------------------------------------
--! @file mul.vhd
--! @brief Simple multiplication.
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

entity mul is
	port 
	(
		in1		: in 	INTEGER;
		in2		: in 	INTEGER;
		result	: out INTEGER
	);
end entity;

architecture mul_arch of mul is
begin

	result <= in1 * in2;
	
end mul_arch;
