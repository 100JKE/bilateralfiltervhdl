-----------------------------------------------------------------------------
--! @file commons.vhd
--! @brief Package encapsulating common definitions used by bilateral filter.
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package commons is

	constant IMAGE_WIDTH			: NATURAL := 300;
	constant IMAGE_HEIGHT		: NATURAL := 300;
	constant MASK_SIZE 			: NATURAL := 5;
	constant MAX_MASK_SIZE		: NATURAL := 15;
	constant EXT_IMAGE_WIDTH	: NATURAL := IMAGE_WIDTH + MASK_SIZE - 1;
	constant EXT_IMAGE_HEIGHT	: NATURAL := IMAGE_HEIGHT + MASK_SIZE - 1;
	constant PIX_TO_PROCESS		: NATURAL := EXT_IMAGE_HEIGHT * EXT_IMAGE_WIDTH;
	constant DATA_SIZE 			: NATURAL := 8;
	constant EXT_DATA_SIZE		: NATURAL := 32;
	constant ADDRESS_SIZE		: NATURAL := 9;
	constant SIGMA_DATA_SIZE 	: NATURAL := 16;
	
	type taps_matrix 	is array ( MASK_SIZE - 1 downto 0 ) of STD_LOGIC_VECTOR ( DATA_SIZE * MASK_SIZE - 1 downto 0 );
	
	type row 			is array ( MASK_SIZE - 1 downto 0 ) of INTEGER;
	type matrix 		is array ( MASK_SIZE - 1 downto 0 ) of row;
	
end commons;

package body commons is

end commons;