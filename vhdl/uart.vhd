-----------------------------------------------------------------------------
--! @file uart.vhd
--! @brief Top level entity for testing bilateral filter while streaming then
--! 		  pixels over UART
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.COMMONS.all;

-----------------------------------------------------------------------------
--! @brief Top level entity. 
--!
--! rx and tx should be maped to pins connected to PC's UART device.
--! clk shuld be maped to internal 50MHz clock
-----------------------------------------------------------------------------
entity uart is
	port
	(
		--! Clock (map to 50MHz internal clock)
		clk								: in 	STD_LOGIC;
		--! Uart Rx chanel
		rx 								: in 	STD_LOGIC;
		--! Uart Tx chanel
		tx 								: out STD_LOGIC

	);
end uart;

architecture uart_arch of uart is

	component uart_ip_streaming is
		port (
			clk             			: in  STD_LOGIC;
			reset           			: in  STD_LOGIC;
			UART_RXD        			: in  STD_LOGIC;
			UART_TXD        			: out STD_LOGIC;
			from_uart_ready 			: in  STD_LOGIC;
			from_uart_data  			: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
			from_uart_error 			: out STD_LOGIC;
			from_uart_valid 			: out STD_LOGIC;
			to_uart_data    			: in  STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
			to_uart_error   			: in  STD_LOGIC;
			to_uart_valid   			: in  STD_LOGIC;
			to_uart_ready   			: out STD_LOGIC
		);
	end component uart_ip_streaming;
	
	component bilateral is
	port 
	(
		clk								: in	STD_LOGIC;
		sigma_s							: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_r							: in	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		data_in							: in	STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
		data_out							: out STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 )
	);
	end component bilateral;
	
	--! Helper signal used to input pixels to bilateral filter
	signal 	read_data 				: STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
	--! Helper signal used to output processed pixels from bilateral filter
	signal 	write_data 				: STD_LOGIC_VECTOR ( DATA_SIZE - 1 downto 0 );
	
	signal 	received_data_enable	: STD_LOGIC := '1';
	signal 	transmit_enable 		: STD_LOGIC := '0';
	signal 	received_data_valid 	: STD_LOGIC := '0';
	signal 	transmit_ready 		: STD_LOGIC := '0';
				
	--! Signal wihich has rising edge only when new pixel arrives via UART
	signal 	enable_shift_in_clk	: STD_LOGIC := '0';
	signal	count						: INTEGER	:= 0;
	
	--! Standard deviation sigma_s input signal (this can be maped as input port when externaly defined)
	signal	i_sigma_s				: STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 ) := STD_LOGIC_VECTOR( TO_UNSIGNED( 182, SIGMA_DATA_SIZE ) );
	--! Standard deviation sigma_r input signal (this can be maped as input port when externaly defined)
	signal	i_sigma_r				: STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 ) := STD_LOGIC_VECTOR( TO_UNSIGNED( 96, SIGMA_DATA_SIZE ) );
	
begin

	-----------------------------------------------------------------------------
	--! Instance of Altera's UART IP component used for communication
	-----------------------------------------------------------------------------
	GENERATE_UART_IP :	component uart_ip_streaming
									port map
									(
										clk             	=> clk,         
										reset           	=> '0',           
										UART_RXD        	=> rx,        
										UART_TXD        	=> tx,        
										from_uart_ready 	=> received_data_enable, 
										from_uart_data  	=> read_data,                          
										from_uart_error 	=> open,                            
										from_uart_valid 	=> received_data_valid,                       
										to_uart_data    	=> write_data,    
										to_uart_error   	=> '0',                              
										to_uart_valid   	=> transmit_enable,                              
										to_uart_ready   	=> transmit_ready                               
									);
		
	-----------------------------------------------------------------------------
	--! Instance of Bilateral filter.
	-----------------------------------------------------------------------------
	GENERATE_BILATERAL:	component bilateral
									port map
									(
										clk					=> enable_shift_in_clk,
										sigma_s				=> i_sigma_s,
										sigma_r				=> i_sigma_r,
										data_in				=> read_data,
										data_out				=> write_data
									);
	
	-----------------------------------------------------------------------------
	--! Process used to generate signal to enable the shift in register only when new pixel arives.
	-----------------------------------------------------------------------------
	ENABLE_SHIFT_IN_SIGN: process( received_data_enable, count )
	begin
		if( received_data_enable = '1' ) then
			enable_shift_in_clk <= '1';
			count <= count + 1;
		else
			enable_shift_in_clk <= '0';
		end if;
	end process;
	
	-----------------------------------------------------------------------------
	--! Process used to communicate pixels via UART in streaming manner.
	-----------------------------------------------------------------------------	
	STREAMING_OF_PIXELS: process( clk )
	begin
		if( clk'event and clk = '1' ) then
			if( received_data_valid = '1' ) then
				transmit_enable <= '1';
				received_data_enable <= '0';
			elsif( transmit_ready = '1' ) then
					received_data_enable <= '1';
					transmit_enable <= '0';
			else
				transmit_enable <= '1';
				received_data_enable <= '0';
			end if;
		end if;
	end process;

end architecture uart_arch;