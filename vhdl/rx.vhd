library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all; -- For evaluation of log2

entity rx is
	generic
	(
		CLK_FREQ : integer := 100; -- Hz
		BAUD_RATE : integer := 10 -- bps
	);
	
	port
	(
		clk :in std_logic := '0';
		rx	: in std_logic := '0'; 
		reset : in std_logic := '0';
		received_byte	: out std_logic_vector (7 downto 0) := "00000000";
		ready	: out std_logic := '0'
	);
end rx;

architecture rx_arch of rx is
	type fsm_state is (state_init, state_receiving);
	signal curr_state, next_state : fsm_state;
	
	constant SAMPLING_FREQ_DIVISOR : integer := CLK_FREQ / BAUD_RATE;
	constant COUNTER_BITS : integer := integer(ceil(log2(real(SAMPLING_FREQ_DIVISOR))));
	constant COUNTER_EVENT_VALUE : integer := integer(ceil( real(SAMPLING_FREQ_DIVISOR) / real(2)));
	signal counter : std_logic_vector(COUNTER_BITS-1 downto 0) := (others => '0');
	
	signal current_bit_index : integer := 0;
	
	constant NUM_OF_BITS_IN_PACKAGE : integer := 10;
	signal data : std_logic_vector (NUM_OF_BITS_IN_PACKAGE - 1 downto 0) := (others => '0');
	signal ready_i : std_logic := '1';

begin
	transition: process (clk, reset)
	begin
		if (reset = '1') then
			curr_state <= state_init;
		elsif (clk'event and clk = '1') then
			curr_state <= next_state;
		end if;
	end process transition;
	
	next_state_logic: process (curr_state, clk, rx)
	begin
		case curr_state is
			when state_init =>
				-- Start on falling edge (start bit)
				if (rx = '0') then
					counter <= (1 => '1', others => '0');
					next_state <= state_receiving;
				end if;
				ready_i <= '0';
			when state_receiving =>
				if (clk'event and clk = '1') then
					
					if (unsigned(counter) = COUNTER_EVENT_VALUE - 1) then
						data(current_bit_index) <= rx;
						
						if (current_bit_index < NUM_OF_BITS_IN_PACKAGE - 1) then
							current_bit_index <= current_bit_index + 1;
							next_state <= state_receiving;
						else
							next_state <= state_init;
							ready_i <= '1';
						end if;
					end if;
					
					if (unsigned(counter) = SAMPLING_FREQ_DIVISOR - 1) then
						counter <= (others => '0');
					else
						counter <= std_logic_vector(unsigned(counter) + 1);
					end if;
					
				end if;
			end case;
	end process next_state_logic;
	
	output_sync: process (curr_state, ready_i, data)
	begin
			received_byte <= data(NUM_OF_BITS_IN_PACKAGE - 2 downto 1);
			ready <= ready_i;
	end process output_sync;
	

	
end rx_arch;
