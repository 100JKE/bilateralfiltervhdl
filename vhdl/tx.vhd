library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all; -- For evaluation of log2

entity tx is
	generic
	(
		CLK_FREQ : integer := 4; -- Hz
		BAUD_RATE : integer := 1 -- bps
	);
	
	port
	(
		clk :in std_logic := '0';
		reset : in std_logic := '0';
		
		data	: in std_logic_vector (7 downto 0) := "00000000"; 
		start : in std_logic := '0';
		ready : out std_logic := '0';
		
		tx : out std_logic := '0'
	);
end tx;

architecture tx_arch of tx is
	type fsm_state is (state_init, state_sending);
	signal curr_state, next_state : fsm_state;

	constant SAMPLING_FREQ_DIVISOR : integer := CLK_FREQ / BAUD_RATE;
	constant COUNTER_BITS : integer := integer(ceil(log2(real(SAMPLING_FREQ_DIVISOR))));
	
	constant IDLE_BIT : std_logic := '1';
	constant START_BIT : std_logic := '0';
	constant STOP_BIT : std_logic := '1';
	
	-- start_bit data_bits(0 to 7) stop_bit
	constant NUM_OF_BITS_IN_PACKAGE : integer := 10;
	
	signal counter : std_logic_vector(COUNTER_BITS-1 downto 0) := (others => '0');
	
	signal current_bit_index : integer := 0;
	signal bit_to_send : std_logic := '1';
	
	signal ready_i : std_logic := '1';
	signal data_to_send : std_logic_vector (NUM_OF_BITS_IN_PACKAGE - 1 downto 0) := (others => '0');

begin
	transition: process (clk, reset)
	begin
		if (reset = '1') then
			curr_state <= state_init;
		elsif (clk'event and clk = '1') then
			curr_state <= next_state;
		end if;
	end process transition;
	
	next_state_logic: process (curr_state, start, clk, data)
	begin
		case curr_state is
			when state_init =>
				current_bit_index <= 0;
				
				if (start = '1') then
					data_to_send <= START_BIT & data & STOP_BIT;
					ready_i <= '0';
					
					-- Send start bit imediatly for zero delay
					-- after start
					bit_to_send <= START_BIT;
					counter <= (1 => '1', others => '0');
					
					next_state <= state_sending;
				else
					bit_to_send <= IDLE_BIT;
					ready_i <= '1';
					counter <= (others => '0');

					next_state <= state_init;
				end if;
			when state_sending =>
				if (clk'event and clk = '1') then
					
					bit_to_send <= data_to_send(current_bit_index);
					
					if (unsigned(counter) = SAMPLING_FREQ_DIVISOR - 1) then
						
						counter <= (others => '0');

						if (current_bit_index < NUM_OF_BITS_IN_PACKAGE - 1) then
							current_bit_index <= current_bit_index + 1;
							next_state <= state_sending;
						else
							next_state <= state_init;
							ready_i <= '1';
						end if;
						
					else
						counter <= std_logic_vector(unsigned(counter) + 1);
						next_state <= state_sending;
					end if;
					
				end if;
		end case;
	end process next_state_logic;
	
	output_sync: process (curr_state, clk)
	begin
		if(clk'event and clk = '1') then
			tx <= bit_to_send;
		end if;
	end process output_sync;
	
	-- Ready is pushed to the output in async maner
	-- since start is async signal also
	ready <= ready_i;


end tx_arch;