-----------------------------------------------------------------------------
--! @file adder_tree.vhd
--! @brief Adder tree used to sum up a row of a matrix
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.MATH_REAL.all;
use WORK.COMMONS.all;

-----------------------------------------------------------------------------
--! @brief Adder three for summing up a array of data.
--!
--! Implemenation is fully generic and can work for arbitrary size of input
--! array.
-----------------------------------------------------------------------------
entity adder_tree is
	port 
	(
		in_array			: in 	row;
		sum				: out INTEGER
	);
end entity;

architecture adder_tree_arch of adder_tree is

	constant first_layer	: INTEGER := 2 ** INTEGER( ceil( log2( real( MASK_SIZE ) ) ) );
	constant num_layers 	: INTEGER := INTEGER( ceil( log2( real( MASK_SIZE ) ) ) );

	type temp_row is array ( first_layer - 1 downto 0 ) of INTEGER;

begin

	process( in_array )
	variable temp		: temp_row := ( others => 0 );
	begin

		for I in 0 to MASK_SIZE - 1 loop
			temp( I ) := in_array( I );
		end loop;
		
		for I in 0 to num_layers - 1 loop
			for J in 0 to 2 ** ( num_layers - 1 - I ) - 1 loop
				temp( J ) := temp( 2 * J ) + temp( 2 * J + 1 );
			end loop;
		end loop;
		
		sum <= temp(0);
		
	end process;

end adder_tree_arch;
