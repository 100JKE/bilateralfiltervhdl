-----------------------------------------------------------------------------
--! @file sigma_scaling.vhd
--! @brief Rescaling of inputes standard deviations.
--!
--! @author Vladimir Polovina
--! @author Milos Stojanovic
--!
--! @date 8 Jun 2016
-----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.MATH_REAL.all;
use WORK.COMMONS.all;

entity sigma_scaling is
	port 
	(
		sigma_s							: in 	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_r							: in 	STD_LOGIC_VECTOR ( SIGMA_DATA_SIZE - 1 downto 0 );
		sigma_s_scaled					: out INTEGER;
		sigma_r_scaled 				: out INTEGER	
	);
end entity;

architecture sigma_scaling_arch of sigma_scaling is

	component divide_ip is
	port
	(
		denom								: in 	STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
		numer								: in 	STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
		quotient							: out STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
		remain							: out STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 )
	);
	end component divide_ip;
	
	-- 255 * 255 * 255
	constant	NUMERATOR_SCALE 		: INTEGER := 16581375;
	
	signal 	two_sigma_r_squared 	: INTEGER;
	signal 	two_sigma_s_squared 	: INTEGER;
	signal 	i_quotient_s 			: STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
	signal 	i_quotient_r 			: STD_LOGIC_VECTOR ( EXT_DATA_SIZE - 1 downto 0 );
	
begin
	two_sigma_s_squared <= 2 * TO_INTEGER( UNSIGNED( sigma_s ) ) * TO_INTEGER( UNSIGNED( sigma_s ) );
	two_sigma_r_squared <= 2 * TO_INTEGER( UNSIGNED( sigma_r ) ) * TO_INTEGER( UNSIGNED( sigma_r ) );

	GENERATE_DIVIDER_S :	component divide_ip
									port map 
									(
										denom	 						=> STD_LOGIC_VECTOR( TO_UNSIGNED( two_sigma_s_squared, 32 ) ),
										numer	 						=> STD_LOGIC_VECTOR( TO_UNSIGNED( NUMERATOR_SCALE, 32 ) ),
										quotient 					=> i_quotient_s,
										remain 						=> open
									);		
	
	GENERATE_DIVIDER_R :	component divide_ip
									port map 
									(
										denom	 						=> STD_LOGIC_VECTOR( TO_UNSIGNED( two_sigma_r_squared, 32 ) ),
										numer	 						=> STD_LOGIC_VECTOR( TO_UNSIGNED( NUMERATOR_SCALE, 32 ) ),
										quotient 					=> i_quotient_r,
										remain 						=> open
									);
									
	sigma_s_scaled <= TO_INTEGER( UNSIGNED( i_quotient_s ) );
	sigma_r_scaled <= TO_INTEGER( UNSIGNED( i_quotient_r ) );
	
end sigma_scaling_arch;
